/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */

define(['N/ui/serverWidget'],
    function(serverWidget) {

/**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2015.2
     */
        function beforeLoad(context) {
        	if (context.type == 'edit'){
        		var currentForm = context.form
        		currentForm.clientScriptFileId = 6726515;
        		currentForm.addButton({
        			id:'custpage_buttonid',
        			label:'process JSON',
        			functionName:'processJSON()'
        		})
        	}
        }
        return {
            beforeLoad: beforeLoad
        };

    });