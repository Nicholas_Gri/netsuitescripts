/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope SameAccount
 */
/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 *   1.0    24.11.2018      N.Grigoriev

 The RESTlet receives JSON with the object that described what object to look at, it's id(external or internal)
 For now we assume, that external id is unique, and will return only 1st value
 List of fields to return, list of sublists to return in array

 Example:

{
  "searchData": {
    "objectType": "SALES_ORDER",
    "internalId": 3790026,
    "externalId": null
  },
  "fieldsToReturn": [
    "trandate",
    "department"
  ],
  "sublists": [
    {
      "sublistName": "item",
      "fieldsToReturn": [
        "item",
        "description"
      ]
    },
    {
      "sublistName": "accountingbookdetail",
      "fieldsToReturn": [
        "accountingbook",
        "exchangerate"
      ]
    }
  ]
}



*/
define(['N/error', 'N/record', 'N/runtime', 'N/email', 'N/search'],
    function(error, record, runtime, email, search) {
        var EXCEPTIONS_LIST = [
            'INVALID_REQUEST',
            'NO_RECOD_TYPE',
            'NO_SUBLIST_TYPE',
            'NO_ID_PROVIDED'
        ];

        /**
         * Function called upon sending a POST request to the RESTlet.
         *
         * @param {string | Object} requestBody - The HTTP request body; request body will be passed into function as a string when request Content-Type is 'text/plain'
         * or parsed into an Object when request Content-Type is 'application/json' (in which case the body must be a valid JSON)
         * @returns {string | Object} HTTP response body; return string when request Content-Type is 'text/plain'; return Object when request Content-Type is 'application/json'
         */
        function doPost(requestBody) {
            var answer ={};
            try {
                log.debug('doPost requestBody', requestBody);
                validateRequest(requestBody);
                var recordObj;
                if (requestBody.searchData.internalId) {
                    var searchObj = requestBody.searchData.objectType;
                    //check if record type is custom or standard
                    var recordType = searchObj.substring(0, 12) == 'customrecord' ? searchObj : record.Type[searchObj];
                    recordObj = getRecordById(requestBody.searchData.internalId, recordType);
                }
                answer.searchData        = requestBody.searchData;
                answer.fieldsToReturn    = getFieldsFromRecord(requestBody.fieldsToReturn,recordObj);
                answer.sublists          = getSublistsFields(requestBody.sublists,recordObj);
                answer.message = 'success';
            } catch (ex) {
                log.error({
                    title: 'doPost',
                    details: ex
                });
                if (EXCEPTIONS_LIST.indexOf(ex.name) < 0) {
                    throw ex;
                }
                answer = {};
                answer.error = {};
                answer.error.code = ex.name;
                answer.error.message = ex.message;
            }
            log.audit('response', answer);
            return answer;
        }

        /**
         * loads NS record using internal id and record type from requestBody
         * @param recordId {number}, recordType {record.Type}
         * @returns record.record object
         */
        function getRecordById(recordId, recordType) {
            log.debug('recordType', recordType);
            var recordObj = record.load({
                type: recordType,
                id: recordId
            });
            //log.debug('getRecordById',recordObj);
            return recordObj;
        }

        /**
         * iterates through an array of fields to return from request and builds object with values from NS object
         * @param fieldsToReturn [],recordObj {record.record}
         * @returns object
         */
        function getFieldsFromRecord(fieldsToReturn,recordObj){
            var fields = {};
            log.debug('fieldsToReturn',fieldsToReturn);
            for (var field in fieldsToReturn){
                fields[fieldsToReturn[field]] = recordObj.getValue({fieldId:fieldsToReturn[field]});
            }
            return fields;
        }

        /**
         * iterates through an array sublists and fields to return for them from request and builds object with values from NS object
         * @param sublists [],recordObj - recordObj {record.record}
         * @returns object
         */
        function getSublistsFields(sublists,recordObj){
            var sublistsArr = [];
            for (var i in sublists){
                var sublistName = sublists[i].sublistName;
                var sublistObj = {};
                sublistObj.sublistName = sublistName;
                sublistObj.lines = [];
                var arrOfFields = sublists[i].fieldsToReturn;
                log.debug('arrOfFields',arrOfFields);
                var lineCount = recordObj.getLineCount({
                    sublistId:sublistName
                });
                for (var lineNum = 0; lineNum<lineCount;lineNum++ ){
                    var lineObj = {};
                    lineObj.lineId = lineNum;
                    for (var j in arrOfFields){
                        log.debug('getting arrOfFields[j]',arrOfFields[j]);
                        lineObj[arrOfFields[j]] = recordObj.getSublistValue({
                            sublistId:sublistName,
                            line:lineNum,
                            fieldId:arrOfFields[j]
                        });
                    }
                    log.debug('pushing lineObj',lineObj);
                    sublistObj.lines.push(lineObj);
                }
                log.debug('pushing sublistObj',sublistObj);
                sublistsArr.push(sublistObj);
                log.debug('sublistsArr',sublistsArr);
            }
            return sublistsArr;
        }

        /**
         * Parses and validates the request's body.
         * @param requestBody
         * @returns parsed request
         * @throws INVALID_REQUEST Request is not formatted properly, or if key fields are missing
         */
        function validateRequest(requestBody) {
            if (!requestBody) {
                log.error('validateRequest', 'No request body found');
                throw error.create({
                    name: 'INVALID_REQUEST',
                    message: 'There was an error with the request'
                });
            }
            if (!requestBody.searchData) {
                log.error('validateRequest', 'No request body found');
                throw error.create({
                    name: 'INVALID_REQUEST',
                    message: 'There was an error with the request'
                });
            }
            if (!requestBody.searchData.objectType) {
                log.error('validateRequest', 'No objectType to search provided');
                throw error.create({
                    name: 'NO_RECOD_TYPE',
                    message: 'There was no record type to search provided'
                });
            }
            if (!requestBody.searchData.internalId && requestBody.searchData.externalId == null) {
                log.error('validateRequest', 'No internal or external id provided');
                throw error.create({
                    name: 'NO_ID_PROVIDED',
                    message: 'There was no internal or external id provided'
                });
            }
            if (!requestBody.fieldsToReturn || requestBody.fieldsToReturn.length == 0) {
                log.error('validateRequest', 'No fields to search provided');
                throw error.create({
                    name: 'NO_SEARCH_FIELDS',
                    message: 'There was no search fields in the request'
                });
            }
            if (requestBody.sublists && requestBody.sublists.length > 0) {
                var sublists = requestBody.sublists;
                sublists.forEach(function(sublist) {
                    if (!sublist.sublistName) {
                        log.error('validateRequest', 'No sublist type to search provided');
                        throw error.create({
                            name: 'NO_SUBLIST_TYPE',
                            message: 'There was no sublist type to search provided'
                        });
                    }
                });
            }
        }

        return {
            post: doPost
        };

    });
