/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/ui/serverWidget', 'N/runtime', 'N/url', 'N/search'],

    function(serverWidget, runtime, url, search) {

        /**
         * Definition of the Suitelet script trigger point.
         *
         * @param {Object} context
         * @param {ServerRequest} context.request - Encapsulation of the incoming request
         * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
         * @Since 2015.2
         */
        function onRequest(context) {
            if (context.request.method === 'GET') {
                displayUI(context);
            } else {
                processRequest(context);
            }
        }

        function displayUI(context) {
            var form = serverWidget.createForm({
                title: 'SavedSearchesSuitelet'
            });
            buildCommonFormElements(form);
            context.response.writePage(form);
        }

        function processRequest(context) {
            var form = serverWidget.createForm({
                title: 'SavedSearchesSuitelet'
            });
            buildCommonFormElements(form);
            try {
                var searchId = context.request.parameters.custpage_search_select;
                var searchobj = search.load({
                    id: searchId
                });
                var searchFiltersArr = searchobj.filters;
                var searchColumnsArr = searchobj.columns;

                var filtersField = form.addField({
                    id: 'custpage_search_filters',
                    type: serverWidget.FieldType.LONGTEXT,
                    label: 'Search Filters'
                }).defaultValue = JSON.stringify(searchFiltersArr);
                var columnsField = form.addField({
                    id: 'custpage_search_colmns',
                    type: serverWidget.FieldType.LONGTEXT,
                    label: 'Search Columns'
                }).defaultValue = JSON.stringify(searchColumnsArr);
                var fullSearchField = form.addField({
                    id: 'custpage_search_fullobj',
                    type: serverWidget.FieldType.LONGTEXT,
                    label: 'Full Search'
                }).defaultValue = JSON.stringify(searchobj);
            } catch (ex) {
                form.addField({
                    id:'custpage_error',
                    type:serverWidget.FieldType.LONGTEXT,
                    label: 'Suitescript Error'
                }).defaultValue = JSON.stringify(ex);
            }

            context.response.writePage(form);

        }

        function buildCommonFormElements(form) {
            var searchField = form.addField({
                id: 'custpage_search_select',
                type: serverWidget.FieldType.SELECT,
                label: 'Saved Search',
                source: 'savedsearchlist'
            });
            fillInSearchFieldOptions(searchField);
            form.addSubmitButton({
                label: 'Get Search Data'
            });
        }

        function fillInSearchFieldOptions(searchField) {
            var searchObj = search.create({
                type: search.Type.SAVED_SEARCH,
                columns: [{
                    name: 'title'
                }]
            });
            var searchid = 0;
            var searchResult = searchObj.run();
            do {
                var resultSlice = searchResult.getRange({
                    start: searchid,
                    end: searchid + 1000
                });
                if (resultSlice && resultSlice.length > 0) {
                    for (var i = 0; i < resultSlice.length; i++) {
                        searchField.addSelectOption({
                            value: resultSlice[i].id,
                            text: resultSlice[i].getValue({
                                name: 'title'
                            })
                        });
                        searchid++;
                    }
                }
            } while (resultSlice.length >= 1000);
        }

        return {
            onRequest: onRequest
        };

    });