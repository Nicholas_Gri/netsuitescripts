/**
 * @NApiVersion 2.0
 */
define(['N/currentRecord', 'N/url', 'N/https', 'N/search'], function(currentRecord, url, https, search) {

    function processJSON() {
        console.log('called client script!');
        var recordObj = currentRecord.get();
        var JSONData = recordObj.getValue({fieldId:'custrecordtest_json'});
        console.log('record ' + recordObj);
        console.log('JSONData ' + JSONData);
        

        var scriptUrl = url.resolveScript({
            scriptId: 'customscriptns_records_restlet',
            deploymentId: 'customdeployns_records_restlet',
            returnExternalUrl: false
        })
        console.log('script url ' + scriptUrl);        
       
        console.log('CurrentRecord.id ' + recordObj.id);
        var response = https.post({
            url: scriptUrl,
            body: JSON.parse(JSONData)
        })
        console.log(response);
        

    }

    function pageInit(scriptContext) {

    }

    return {
        processJSON: processJSON,
        pageInit: pageInit
    }
})